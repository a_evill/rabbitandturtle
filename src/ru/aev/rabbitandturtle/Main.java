package ru.aev.rabbitandturtle;

public class Main {
    public static void main(String[] args) {
        AnimalThread turtle = new AnimalThread("Черепаха", 1);
        AnimalThread rabbit = new AnimalThread("Кролик", 10);

        turtle.start();
        rabbit.start();

    }
}
