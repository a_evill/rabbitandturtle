package ru.aev.rabbitandturtle;

class AnimalThread extends Thread{
    AnimalThread(String name, int priority) {
        setName(name);
        setPriority(priority);
    }

    public void run() {
        for (int i = 0; i <= 30_000; i++) {
            if (i % 500 == 0) {
                System.out.println(getName() + " на расстоянии: " + i + "м");
            }
            if (i == 5_000 && currentThread().getPriority() == 10) {
                setPriority(Thread.MIN_PRIORITY);
            } else if (i == 5_000 && currentThread().getPriority() == 1) {
                setPriority(Thread.MAX_PRIORITY);
            }
        }
    }
}

